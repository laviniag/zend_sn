<?php
/**
 * Created by PhpStorm.
 * User: daniele
 * Date: 20/09/2017
 * Time: 21:32
 */

namespace Users\Service;

use Zend\Authentication\Result;
use Zend\Db\Adapter\AdapterInterface;
use Users\Entity\User;
use Zend\Db\Sql\Sql;

/**
 * Adapter used for authenticating user. It takes login and password on input
 * and checks the database if there is a user with such login (email) and password.
 * If such user exists, the service returns his identity (email). The identity
 * is saved to session and can be retrieved later with Identity view helper provided
 * by ZF3.
 */

class AuthAdapter
{
    private $username = null;
    private $password = null;
    private $dbAdapter;

    /**
     * Constructor.
     */
    public function __construct(AdapterInterface $adapter)
    {
        $this->dbAdapter = $adapter;
    }

    /**
     * Sets username.
     */
    public function setUsername($username)
    {
        $this->username = $username;
        return $this;
    }

    /**
     * Sets password.
     */
    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }

    /**
     * Performs an authentication attempt.
     */
    public function authenticate()
    {
        // Check the database if there is a user with such email.
        $sql = new Sql($this->dbAdapter);
        $select = $sql->select('users_sn');
        $select->where(['username' => $this->username]);

        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();

        // If there is no such user, return 'Identity Not Found' status.
        if (empty($rowset->current())) {
            return new Result(
                Result::FAILURE_IDENTITY_NOT_FOUND,
                'username',
                ['Invalid credentials.']);
        }

        // If the user with such email exists, we need to check if it is active or retired.
        // Do not allow retired users to log in.
        $user = new User();

//        if ($user->getStatus()==User::STATUS_RETIRED) {
//            return new Result(
//                Result::FAILURE,
//                null,
//                ['User is retired.']);
//        }

        // Now we need to compare user-entered password and the password decrypted
        $user->exchangeArray($rowset->current());
        if($user->getPassword() == $this->password) {
            // Great! The password hash matches. Return user identity (email) to be
            // saved in session for later use.
            return new Result(
                Result::SUCCESS,
                $this->username,
                ['Authenticated successfully.']);
        }

        // If password check didn't pass return 'Invalid Credential' failure status.
        return new Result(
            Result::FAILURE_CREDENTIAL_INVALID,
            'password',
            ['Invalid password.']);
    }
}