<?php
/*
 * Helper functions for building a DataTables server-side processing SQL query
 *
 * Performs a Jquery DataTables with Server-Side Processing 
 * Implemented Databases: Mysql + PostgreSQL
 * Options:
 *    columns - join - where - group by - order by
 * Main method:
 *    fetch()
 *
 * Usage:
 * Javascript:
 *  $('#example').dataTable({
        "processing": true,
        "serverSide": true,
        "ajax": "serverSideTable",
        "columns": [
            { "data": "id", "searchable": false },
            { "data": "code" },
            { "data": "name", "orderable": false }
        ]
    });
 * PHP code:
 *   public function serverSideTable () {
        $server_side = new TableSSP("pgsql");
        $data = $server_side->table('user')->join('names','user_id','name_id')
                            ->columns("user.id, names.code, names.name")
                            ->where("name = 'Jane'")->fetch();
        echo json_encode($data);
    }
 * Database interface:
 *     Illuminate\Database
 */
 
namespace    App\Assets;

use \DB as Capsule;

class TableSSP 
{
    protected    $set_db;
    protected    $set_table;
    protected    $set_what;
    protected    $set_cols = [];
    protected    $set_join;
    protected    $set_where;
    protected    $set_args = [];
    protected    $set_group;
    protected    $set_order;
    
    /* Constructor:
     * Set the Database Type: 'mysql' (default) / 'pgsql': postgreSQL
     */
    public function __construct ( $db_type = null )
    {
        if ( $db_type ) {
            $this->set_db = $db_type;
        } else {
            $this->set_db = 'mysql';
        }
    }
    
    /* Assign Table:
     * Set the main Table - always required
     *  $table = the table name
     */
    public function table ( $table_name )
    {    
        $this->set_table = $table_name;
        return $this;
    }   
    
    /* Assign Columns:
     * Set the Column names: if missing, defaults to the javascript columns
     *  $columns_list = string containing the column list:
     *  allowed syntax: "id, code" - "user.id, user.code" - "user.id AS key, code as serial_number"
     */  
    public function columns ( $columns_list )
    {    
        $this->set_what = $columns_list;
        return $this;
    }    
    
    /* Assign Join with Table:
     * Set the joined Table, primary key and foreign key, join type
     *  $table_join = the joined table name
     *  $p_key = the primary key (first table)
     *  $f_key = the foreign key (joined table)
     *  $join_type = the join type: 'inner', 'left', 'right'
     */
    public function join ( $table_join, $p_key, $f_key, $join_type='INNER' )
    {    
        $this->set_join = "$join_type JOIN $table_join ON $this->set_table.$p_key = $table_join.$f_key";
        return $this;
    }    
    
    /* Assign Where:
     * Set the Where condition
     *  $where_cond = string contaning the condition, without the word 'where'
     */
    public function where ( $where_cond )
    {    
        $this->set_where = 'WHERE '.$where_cond;
        return $this;
    }   
    
    /* Assign Group by:
     * Set the Group by condition
     *  $group_list = string contaning the list to group by, without the word 'group by'
     */
    public function groupby ( $group_list )
    {    
        $this->set_group = 'GROUP BY '.$group_list;
        return $this;
    }      
    
    /* Assign Order by:
     * Set the Order by condition
     *  $order_list = string contaning the list to order by, without the word 'order by': 
     *  this condition is valid only at the initial state of the table
     */
    public function orderby ( $order_list )
    {    
        $this->set_order = 'ORDER BY '.$order_list;
        return $this;
    } 
    
    /* Fetch the Table content:
     * Perform the SQL queries needed for a server-side processing request.
     *  @return array = Server-side processing response array
     */
    public function fetch ( )
    {
        $join = ($this->set_join) ? $this->set_join : '';
        $group = ($this->set_group) ? $this->set_group : '';

        //     Build the SQL query string from the request
        $binding = array();
        $what = $this->getColumns( );
        $limit = $this->getLimit( );
        $order = $this->getOrder( );
        
        $where = ($this->set_where) ? $this->set_where : '';
        $filter = ($what <> '*') ? $this->getFilter($binding) : '';
        if ($where == '' && $filter <> '') {
            $filter = 'WHERE '.$filter;
        } elseif ($filter <> '') {
            $where = str_replace("WHERE ","WHERE (",$where).')';
            $filter = 'AND '.$filter;
        }
        
        //     Switch among the database types
        switch ( $this->set_db ) {
            case 'mysql':
                $rows_query = "SELECT COUNT(*) AS count FROM $this->set_table $join $where $group";
                $filt_query = "SELECT COUNT(*) AS count FROM $this->set_table $join $where $filter $group";
                $data_query = "SELECT $what FROM $this->set_table $join $where $filter $group $order $limit";
                break;
                
            case 'pgsql':
                $rows_query = "SELECT COUNT(*) FROM $this->set_table $join $where $group";
                $filt_query = "SELECT COUNT(*) FROM $this->set_table $join $where $filter $group";
                $data_query = "SELECT $what FROM $this->set_table $join $where $filter $group $order $limit";
                break;
        }
     
//    echo $rows_query.PHP_EOL;
//    echo $data_query.PHP_EOL;
//    die();

        //     Get the total num rows and the filered num rows
        $count = Capsule::select($rows_query);
        $recordsTotal = $count[0]->count;
        if ($filter <> '') {
            $count = Capsule::select($filt_query, $binding);
            $recordsFiltered = $count[0]->count;
        } else {
            $recordsFiltered = $recordsTotal;
        }
        
        //     Fetch table content
        $data = Capsule::select($data_query, $binding);

        //     Output data
        return array(
            "draw"            => $_GET['draw'],
            "recordsTotal"    => $recordsTotal,
            "recordsFiltered" => $recordsFiltered,
            "data"            => $data
        );
    }

    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * Internal methods
     *
     * Columns:
     * Construct the COLUMNS content for server-side processing SQL query
     *  @return string SQL limit clause
     */
    protected function getColumns ( )
    {
        if ( $this->set_what ) 
        {
            $columns = explode(',',$this->set_what);
            foreach ($columns as $item) {
                if ( ($aspos = strpos($item,' as ') + strpos($item,' AS ')) > 0 ) {
                    $item = substr( $item, 0, $aspos );
                }
                $this->set_cols[] = trim($item);
            }
            return $this->set_what;
        } 
        elseif ( isset($_GET['columns']) ) 
        {
            $what = '*';
            foreach ($_GET['columns'] as $column) {
                if (is_numeric($column['data'])) {
                    $this->set_cols[] = intval($column['data']) + 1;
                } else {
                    $this->set_cols[] = $column['data'];
                }
            }
            if (sizeof($this->set_cols) > 0) {
                $what = implode(', ',$this->set_cols);
            }
            return $what;
        }
    }

    /* Paging:
     * Construct the LIMIT clause for server-side processing SQL query
     *  @return string SQL "limit and offset" clause
     */
    protected function getLimit ( )
    {
        $limit = '';

        if ( isset($_GET['start']) && $_GET['length'] != -1 ) {
            switch ( $this->set_db ) {
                case 'mysql':
                    $limit = 'LIMIT '.intval($_GET['start']).', '.intval($_GET['length']);
                    break;
                    
                case 'pgsql':
                    $limit = 'LIMIT '.intval($_GET['length']).' OFFSET '.intval($_GET['start']);
                    break;
            }
        }

        return $limit;
    }

    /* Ordering:
     * Construct the ORDER BY clause for server-side processing SQL query.
     * At the initial state the ORDER BY clause can be set in method "orderby"
     *  @return string SQL "order by" clause
     */
    protected function getOrder ( )
    {
        $order = '';

        if ( isset($_GET['draw']) && $_GET['draw'] == 1 && $this->set_order ) {
            $order = $this->set_order;
        } 
        elseif ( isset($_GET['order']) && count($_GET['order']) ) {
            $columns = array();

            foreach ($_GET['order'] as $item) {
                $columns[] = $this->set_cols[$item['column']].' '.$item['dir'];
            }
            $order = 'ORDER BY '.implode(', ', $columns);
        }
        
        return $order;
    }

    /* Searching / Filtering
     * Construct the WHERE clause in search filter for server-side processing SQL query.
     *  $binding = Array of values for PDO bindings
     *  @return string SQL "where" clause
     */
    protected function getFilter ( &$binding )
    {
        $filter = $glob_search = '';  
        $stack = array();

        if ( isset($_GET['search']) && $_GET['search']['value'] <> '' ) {
            $glob_search = $_GET['search']['value'];
        }
        //  All columns or individual column filtering
        foreach ($_GET['columns'] as $ind => $column) {
            if ($column['searchable'] == 'true') {
                $search = ($glob_search <> '') ? $glob_search : $column['search']['value'];
                if ($search <> '') {
                    $binding[] = '%'.$search.'%';
                    
                    switch ( $this->set_db ) {
                        case 'mysql':
                            $stack[] = 'CONVERT('.$this->set_cols[$ind].',CHAR(255)) LIKE ?';
                            break;
                            
                        case 'pgsql':
                            $stack[] = $this->set_cols[$ind].'::text ILIKE ?';
                            break;
                    }
                }
            }
        }
        if (sizeof($stack) > 0) {
            if ($glob_search <> '') {
                $filter = '('.implode(' OR ', $stack).')';
            } else {
                $filter = implode(' AND ', $stack);
            }
        }

        return $filter;
    }
}
