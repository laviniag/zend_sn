<?php

namespace Users\Validator;

use Zend\Db\Sql\Sql;
use Zend\Validator\AbstractValidator;

class NoRecordExists extends AbstractValidator
{
    const INVALID   = 'NoRecordExists';
    protected $table = 'users_sn';
    protected $field;
    protected $adapter;

    protected $messageTemplates = array();

    public function __construct($options = null)
    {
        $this->field = $options['field'];
        $this->adapter = $options['adapter'];
        $this->messageTemplates = [self::INVALID => "This ".$this->field." already exists!"];
        parent::__construct($options);
    }

    /**
     * Returns true if the given string does not exist in field
     *
     * @param string $value 
     * @return boolean
     */
    public function isValid($value)
    {
        if (empty($value)) {
            $this->error(self::INVALID);
            return false;
        }

        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from($this->table);
        $select->where([$this->field => $value]);

        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        if (!empty($result->current())) {
            $this->error(self::INVALID);
            return false;
        }
        return true;
    }
}