<?php

namespace Users\Entity;

use Wall\Entity\UserImage;
use Wall\Entity\UserLink;
use Wall\Entity\UserStatus;
use Zend\Crypt\BlockCipher;
use Zend\Hydrator\ClassMethods;

class User
{
    const GENDER_MALE = 1;
    
    protected $id;
    protected $username;
    protected $email;
    protected $password;
    protected $name;
    protected $surname;
    protected $avatar;
    protected $bio;
    protected $location;
    protected $gender;
    protected $createdAt = null;
    protected $updatedAt = null;
    protected $feeds = array();
    protected $blockCipher;

    public function __construct()
    {
        $this->blockCipher = BlockCipher::factory('openssl', ['algo' => 'aes']);
        $this->blockCipher->setKey('A*$grt/67KSui90');
    }

    public function exchangeArray(array $data)
    {
        $this->id     = !empty($data['id']) ? $data['id'] : null;
        $this->username = !empty($data['username']) ? $data['username'] : null;
        $this->email  = !empty($data['email']) ? $data['email'] : null;
        $this->password  = !empty($data['password']) ? $data['password'] : null;
        $this->name  = !empty($data['name']) ? $data['name'] : null;
        $this->surname  = !empty($data['surname']) ? $data['surname'] : null;
        $this->avatar  = !empty($data['avatar']) ? $data['avatar'] : null;
        $this->bio  = !empty($data['bio']) ? $data['bio'] : null;
        $this->location  = !empty($data['location']) ? $data['location'] : null;
        $this->gender  = !empty($data['gender']) ? $data['gender'] : null;
        $this->createdAt  = !empty($data['created_at']) ? $data['created_at'] : null;
        $this->updatedAt  = !empty($data['updated_at']) ? $data['updated_at'] : null;
    }

    public function getArrayCopy()
    {
        return [
            'id' => $this->id,
            'username' => $this->username,
            'name' => $this->name,
            'email' => $this->email,
            'password' => $this->password,
            'surname'  => $this->surname,
            'avatar'  => (!empty($this->avatar)) ? $this->username.'/'.$this->avatar : 'avatar.png',
            'bio'  => $this->bio,
            'location'  => $this->location,
            'gender'  => $this->gender,
            'created_at'  => $this->createdAt,
            'updated_at'  => $this->updatedAt,
        ];
    }

    public function getFields($full = false)
    {
        $copy = [
            'username' => $this->getUsername(),
            'email' => $this->getEmail(),
            'password' => $this->setPassword($this->password),
            'name' => $this->getName(),
            'surname' => $this->getSurname(),
            'avatar' => $this->getAvatar(true),
            'bio' => $this->getBio(),
            'location' => $this->getLocation(),
            'gender' => $this->getGender(),
            'created_at' => $this->getCreatedAt(),
        ];
        if ($full !== false) {
            $copy['id'] = $this->getId();
            $copy['updated_at'] = $this->setUpdatedAt();
        }
        return $copy;
    }

    public function setId($id)
    {
        $this->id = (int)$id;
    }

    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @param mixed $email
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @param mixed $password
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $this->blockCipher->encrypt($password);
        return $this->password;
    }

    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    public function setSurname($surname)
    {
        $this->surname = $surname;
        return $this;
    }

    public function setAvatar($avatar)
    {
        $this->avatar = $avatar;
        return $this;
    }

    public function setBio($bio)
    {
        $this->bio = $bio;
        return $this;
    }

    public function setLocation($location)
    {
        $this->location = $location;
        return $this;
    }

    public function setGender($gender)
    {
        $this->gender = (int)$gender;
        return $this;
    }

    public function setFeeds($feeds)
    {
        $hydrator = new ClassMethods();
        foreach ($feeds as $entry) {
            if (array_key_exists('status', $entry)) {
                $this->feeds[] = $hydrator->hydrate($entry, new UserStatus());
            }
            elseif (array_key_exists('filename', $entry)) {
                $this->feeds[] = $hydrator->hydrate($entry, new UserImage($this->username));
            }
            elseif (array_key_exists('url', $entry)) {
                $this->feeds[] = $hydrator->hydrate($entry, new UserLink());
            }
        }
        unset($hydrator);
    }

    public function setCreatedAt($createdAt = null)
    {
        $datetime = new \DateTime($createdAt);
        $this->createdAt = $datetime->format('Y-m-d H:i:s');
        return $this->createdAt;
    }

    public function setUpdatedAt($updatedAt = null)
    {
        $datetime = new \DateTime($updatedAt);
        $this->updatedAt = $datetime->format('Y-m-d H:i:s');
        return $this->updatedAt;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->blockCipher->decrypt($this->password);
    }

    public function getName()
    {
        return $this->name;
    }

    public function getSurname()
    {
        return $this->surname;
    }

    public function getBio()
    {
        return $this->bio;
    }

    public function getLocation()
    {
        return $this->location;
    }

    public function getGender()
    {
        return $this->gender;
    }

    public function getAvatar($justfield = false)
    {
        if ($justfield) {
            return $this->avatar;
        }
        return (!empty($this->avatar)) ? $this->username.'/'.$this->avatar : 'avatar.png';
    }

    public function getGenderString()
    {
        return ($this->gender == self::GENDER_MALE) ? 'Male' : 'Female';
    }

    public function getFeeds()
    {
        return $this->feeds;
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
}