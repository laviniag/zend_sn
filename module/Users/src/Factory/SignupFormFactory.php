<?php
/**
 * Created by PhpStorm.
 * User: daniele
 * Date: 18/09/2017
 * Time: 16:34
 */
namespace Users\Factory;

use Interop\Container\ContainerInterface;
use Zend\Db\Adapter\AdapterInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class MyFormFactory
 * @package MyModule\Factory\Form
 */
class SignupFormFactory implements FactoryInterface
{

    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        /** @var \Zend\Db\Adapter\AdapterInterface $adapter */
        $adapter  = $container->get(AdapterInterface::class);

        return new SignupForm($adapter);
    }
}