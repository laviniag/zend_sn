<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Users\Controller;

use Application\Helper\Image;
use Application\Mailer;
use Users\Entity\User;
use Users\Forms\LoginForm;
use Users\Forms\SignupForm;
use Users\Model\UsersTable;
use Users\Service\AuthAdapter;
use Zend\Db\Adapter\AdapterInterface;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Session\Container;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

class UsersController extends AbstractActionController
{
    private $userTable;
    private $dbAdapter;

    public function __construct(UsersTable $user_table, AdapterInterface $adapter)
    {
        $this->userTable = $user_table;
        $this->dbAdapter = $adapter;
    }

    public function indexAction()
    {
        $auth_session = new Container('Auth_session');
        $this->layout()->username = (isset($auth_session->user)) ? $auth_session->user : '';

        return new ViewModel();
    }

    public function loadAction()
    {
        $this->layout('');

        $userList = $this->userTable->getList();
        return new JsonModel($userList);
    }

    public function loginAction()
    {
        $viewData = array();
        $auth_session = new Container('Auth_session');
        $this->layout()->username = (isset($auth_session->user)) ? $auth_session->user : '';

        $loginForm = new LoginForm($this->dbAdapter);
        $loginForm->setAttribute('action', $this->url()->fromRoute('login'));

        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost()->toArray();
            $username = $data['username'];
            $password = $data['password'];

            $loginForm->setData($data);
            if ($loginForm->isValid()) {
                $auth = new AuthAdapter($this->dbAdapter);
                $auth->setUsername($username)->setPassword($password);
                $result = $auth->authenticate();
                if ($result->isValid()) {
                    $user = $this->userTable->getByUsername($username);
//                    $sendMail = new Mailer();
//                    $sendMail->sendWelcomeEmail($user->getEmail(), $user->getName());

                    $auth_session->user = $username;
                    return $this->redirect()->toRoute('wall', ['username' => $username]);
                }
                else {
                    $loginForm->setMessages([$result->getIdentity() => $result->getMessages()]);
                }
            }
        }
        $viewData['loginForm'] = $loginForm;
        return $viewData;
    }

    public function signupAction()
    {
        $viewData = array();
        $this->layout()->username = '';

        $signupForm = new SignupForm($this->dbAdapter);
        $signupForm->setAttribute('action', $this->url()->fromRoute('signup'));

        $request = $this->getRequest();
        if ($request->isPost()) {
            $signupForm = $this->signupSave($request, $signupForm);
        }
        $viewData['signupForm'] = $signupForm;
        return $viewData;
    }

    public function signupSave($request, SignupForm $signupForm)
    {
        if ($request->isPost()) {
            $data = array_merge_recursive(
                $request->getPost()->toArray(),
                $request->getFiles()->toArray()
            );
            $signupForm->setData($data);
            if (! empty($request->getFiles()->avatar['name'])) {
                $signupForm->addFileFilter();
            }
            if ($signupForm->isValid()) {
                $user = new User();
                $user->exchangeArray($signupForm->getData());
                $username = $user->getUsername();

                if (! empty($request->getFiles()->avatar['name'])) {
                    $picture = new Image($username);
                    $user->setAvatar($picture->archive($request->getFiles()->avatar));
                }
                $response = $this->userTable->create($user);
                if ($response == true) {
//                    Mailer::sendWelcomeEmail($user->getEmail(), $user->getName());

                    $this->flashMessenger()->addSuccessMessage('Account created!');
                    $auth_session = new Container('Auth_session');
                    $auth_session->user = $username;
                    return $this->redirect()->toRoute('wall', ['username' => $username]);
                }
            }
//            else {
//                print_r($signupForm->getMessages());
//                die();
//            }
        }
        return $signupForm;
    }

    public function logoutAction ()
    {
        $auth_session = new Container('Auth_session');
        unset($auth_session->user);
        $this->flashMessenger()->addSuccessMessage('You are logged out');
        return $this->redirect()->toRoute('home');
    }
}