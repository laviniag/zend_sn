<?php
namespace Users\Model;

use Users\Entity\User;
use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\TableGatewayInterface;

class UsersTable
{
    private $tableGateway;

    public function __construct(TableGatewayInterface $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function getList()
    {
        $rowset = $this->tableGateway->select(function (Select $select) {
            $select->order('created_at DESC');
        });
        $output = array();
        foreach ($rowset as $row) {
            $output[] = $row->getArrayCopy();
        }
        return $output;
    }

    /**
     * Method to get users by username
     *
     * @param string $username
     * @return ArrayObject
     */
    public function getByUsername($username)
    {
        $rowset = $this->tableGateway->select(['username' => $username]);
        return ($rowset->count() == 1) ? $rowset->current() : false;
    }
    
    /**
     * Method to get users by id
     *
     * @param int $id
     * @return ArrayObject
     */
    public function getById($id)
    {
        $rowset = $this->tableGateway->select(['id' => $id]);
        return ($rowset->count() == 1) ? $rowset->current() : false;
    }

    /**
     * Method to insert a new user
     *
     * @param UserStatus $userStatus
     * @return boolean
     */
    public function create(User $user)
    {
        $user->setCreatedAt();
        $data = $user->getFields();

        return $this->tableGateway->insert($data);
    }
}