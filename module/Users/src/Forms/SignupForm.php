<?php
namespace Users\Forms;

use Users\Validator\NoRecordExists;
use Zend\Db\Adapter\AdapterInterface;
use Zend\Filter\StringTrim;
use Zend\Filter\StripTags;
use Zend\Form\Form;
use Zend\InputFilter\FileInput;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Validator\EmailAddress;
use Zend\Validator\Hostname;
use Zend\Validator\Identical;
use Zend\Validator\StringLength;

class SignupForm extends Form implements InputFilterProviderInterface
{
    private $dbAdapter;

    public function __construct(AdapterInterface $adapter)
    {
        parent::__construct('users-signup');
        $this->dbAdapter = $adapter;

        $this->setAttribute('method', 'post');
        $this->setAttribute('class', 'well form-horizontal');
        $this->setAttribute('enctype', 'multipart/form-data');

        $this->prepareElements();
        // Add validation rules
        $this->addInputFilter();
    }

    public function prepareElements()
    {
        $this->add([
            'name' => 'username',
            'type'  => 'text',
            'attributes' => ['class' => 'form-control'],
            'options' => [
                'label' => 'Username',
                'label_attributes' => ['class' => 'control-label col-md-3']
            ]
        ]);
        $this->add([
            'name' => 'email',
            'type'  => 'text',
            'attributes' => ['class' => 'form-control'],
            'options' => [
                'label' => 'Email',
                'label_attributes' => ['class' => 'control-label col-md-3']
            ]
        ]);
        $this->add([
            'name' => 'password',
            'type'  => 'password',
            'attributes' => ['class' => 'form-control'],
            'options' => [
                'label' => 'Password',
                'label_attributes' => ['class' => 'control-label col-md-3']
            ]
        ]);
        $this->add([
            'name' => 'repeat_password',
            'type'  => 'password',
            'attributes' => ['class' => 'form-control'],
            'options' => [
                'label' => 'Repeat password',
                'label_attributes' => ['class' => 'control-label col-md-3']
            ]
        ]);
        $this->add([
            'name' => 'avatar',
            'type'  => 'file',
            'attributes' => ['class' => 'form-control'],
            'options' => [
                'label' => 'Avatar',
                'label_attributes' => ['class' => 'control-label col-md-3']
            ]
        ]);
        $this->add([
            'name' => 'name',
            'type'  => 'text',
            'attributes' => ['class' => 'form-control'],
            'options' => [
                'label' => 'Name',
                'label_attributes' => ['class' => 'control-label col-md-3']
            ]
        ]);
        $this->add([
            'name' => 'surname',
            'type'  => 'text',
            'attributes' => ['class' => 'form-control'],
            'options' => [
                'label' => 'Surname',
                'label_attributes' => ['class' => 'control-label col-md-3']
            ]
        ]);
        $this->add([
            'name' => 'bio',
            'type'  => 'textarea',
            'attributes' => ['class' => 'form-control'],
            'options' => [
                'label' => 'Bio',
                'label_attributes' => ['class' => 'control-label col-md-3']
            ]
        ]);
        $this->add([
            'name' => 'location',
            'type'  => 'text',
            'attributes' => ['class' => 'form-control'],
            'options' => [
                'label' => 'Location',
                'label_attributes' => ['class' => 'control-label col-md-3']
            ]
        ]);
        $this->add([
            'name' => 'gender',
            'type'  => 'radio',
            'attributes' => ['class' => 'form-control'],
            'options' => [
                'label' => 'Gender',
                'label_attributes' => ['class' => 'radio'],
                'value_options' => [
                    0 => 'Female',
                    1 => 'Male'
                ]
            ]
        ]);
        // Add the CSRF field
        $this->add([
            'type'  => 'csrf',
            'name' => 'csrf',
            'options' => [
                'csrf_options' => [
                    'timeout' => 600
                ]
            ],
        ]);
        $this->add([
            'name' => 'register',
            'attributes' => [
                'type'  => 'submit',
                'value' => 'Register',
                'class' => 'btn btn-primary'
            ],
        ]);
    }

    public function addInputFilter()
    {
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);

        // Add validation rules for the "username" field.
        $inputFilter->add([
            'name' => 'username',
            'filters' => [
                ['name' => StripTags::class],
                ['name' => StringTrim::class],
            ],
            'validators' => [
                ['name' => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 3,
                        'max' => 50,
                    ],
                ],
                ['name' => NoRecordExists::class,
                    'options' => [
                        'field' => 'username',
                        'adapter' => $this->dbAdapter
                    ]
                ]
            ]
        ]);
        // Add validation rules for the "email" field.
        $inputFilter->add([
            'name' => 'email',
            'filters' => [
                ['name' => StripTags::class],
                ['name' => StringTrim::class],
            ],
            'validators' => [
                ['name' => EmailAddress::class,
                    'options' => [
                        'allow' => Hostname::ALLOW_DNS,
                        'useMxCheck' => false,
                        'messages' => [EmailAddress::INVALID_FORMAT =>
                            'The input is not a valid email address'
                        ]
                    ],
                ],
                ['name' => NoRecordExists::class,
                    'options' => [
                        'field' => 'email',
                        'adapter' => $this->dbAdapter
                    ]
                ]
            ]
        ]);
        // Add validation rules for the "password" field.
        $inputFilter->add([
            'name' => 'password',
            'filters' => [
                ['name' => StripTags::class],
                ['name' => StringTrim::class],
            ],
            'validators' => [
                ['name' => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 6,
                        'max' => 80,
                    ],
                ]
            ]
        ]);
        $inputFilter->add([
            'name'     => 'repeat_password',
            'filters'  => [
                ['name' => StripTags::class],
                ['name' => StringTrim::class],
            ],
            'validators' => [
                ['name'  => Identical::class,
                    'options' => [
                        'token' => 'password',
                        'messages' => [Identical::NOT_SAME => 'The two passwords do not match'],
                    ],
                ],
            ],
        ]);
    }

    public function addFileFilter()
    {
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);

        // Add validation rules for the "avatar" field.
        $inputFilter->add([
            'type' => FileInput::class,
            'name' => 'avatar',
            'validators' => [
                ['name' => 'FileIsImage'],
                ['name' => 'FileMimeType',
                    'options' => [
                        'mimeType' => ['image/jpeg', 'image/png', 'image/gif']
                    ]
                ],
                ['name' => 'FileImageSize',
                    'options' => [
                        'minWidth' => 128,
                        'minHeight' => 128,
                        'maxWidth' => 4096,
                        'maxHeight' => 4096
                    ]
                ],
            ],
        ]);
    }

    /**
     * Should return an array specification compatible with
     * {@link Zend\InputFilter\Factory::createInputFilter()}.
     *
     * @return array
     */
    public function getInputFilterSpecification()
    {
        return [
            'name' => ['required' => true],
            'surname' => ['required' => true],
            'gender' => ['required' => true],
            'username' => ['required' => true],
            'email' => ['required' => true],
            'password' => ['required' => true],
            'repeat_password' => ['required' => true],
        ];
    }
}