<?php
namespace Users\Forms;

use Users\Validator\RecordExists;
use Zend\Db\Adapter\AdapterInterface;
use Zend\Form\Form;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Filter\StringTrim;
use Zend\Filter\StripTags;
use Zend\Validator\StringLength;

class LoginForm extends Form implements InputFilterProviderInterface
{
    private $dbAdapter;

    public function __construct(AdapterInterface $adapter)
    {
        parent::__construct('users-signup');
        $this->dbAdapter = $adapter;

        $this->setAttribute('method', 'post');
        $this->setAttribute('class', 'well form-horizontal');

        $this->prepareElements();
        // Add validation rules
        $this->addInputFilter();
    }

    public function prepareElements()
    {
        $this->add([
            'name' => 'username',
            'type'  => 'text',
            'attributes' => ['class' => 'form-control'],
            'options' => [
                'label' => 'Username',
                'label_attributes' => ['class' => 'control-label col-md-12 to-left']
            ]
        ]);
        $this->add([
            'name' => 'password',
            'type'  => 'password',
            'attributes' => ['class' => 'form-control'],
            'options' => [
                'label' => 'Password',
                'label_attributes' => ['class' => 'control-label col-md-12 to-left']
            ]
        ]);
        // Add the CSRF field
        $this->add([
            'type'  => 'csrf',
            'name' => 'csrf',
            'options' => [
                'csrf_options' => [
                    'timeout' => 600
                ]
            ],
        ]);
        $this->add([
            'name' => 'login',
            'attributes' => [
                'type'  => 'submit',
                'value' => 'Login',
                'class' => 'btn btn-primary'
            ],
        ]);
    }

    public function addInputFilter()
    {
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);

        // Add validation rules for the "username" field.
        $inputFilter->add([
            'name' => 'username',
            'filters' => [
                ['name' => StripTags::class],
                ['name' => StringTrim::class],
            ],
            'validators' => [
                ['name' => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 3,
                        'max' => 50,
                    ],
                ],
                ['name' => RecordExists::class,
                    'options' => [
                        'field' => 'username',
                        'adapter' => $this->dbAdapter
                    ]
                ]
            ]
        ]);
        // Add validation rules for the "password" field.
        $inputFilter->add([
            'name' => 'password',
            'filters' => [
                ['name' => StripTags::class],
                ['name' => StringTrim::class],
            ],
            'validators' => [
                ['name' => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 6,
                        'max' => 80,
                    ],
                ]
            ]
        ]);
    }

    /**
     * Should return an array specification compatible with
     * {@link Zend\InputFilter\Factory::createInputFilter()}.
     *
     * @return array
     */
    public function getInputFilterSpecification()
    {
        return [
            'username' => ['required' => true],
            'password' => ['required' => true],
        ];
    }
}