<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */
namespace Users;

use Zend\Router\Http\Literal;

return array(
    'router' => [
        'routes' => [
            'users' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/users',
                    'defaults' => [
                        'controller' => Controller\UsersController::class,
                        'action'     => 'index'
                    ],
                ],
            ],
            'load' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/load',
                    'defaults' => [
                        'controller' => Controller\UsersController::class,
                        'action'     => 'load'
                    ],
                ],
            ],
            'login' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/login',
                    'defaults' => [
                        'controller' => Controller\UsersController::class,
                        'action'     => 'login'
                    ],
                ],
            ],
            'signup' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/signup',
                    'defaults' => [
                        'controller' => Controller\UsersController::class,
                        'action'     => 'signup'
                    ],
                ],
            ],
            'logout' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/logout',
                    'defaults' => [
                        'controller' => Controller\UsersController::class,
                        'action'     => 'logout'
                    ],
                ],
            ],
        ]
    ],
    'form_elements' => [
        'factories' => [
            Forms\SignupForm::class => Factory\SignupFormFactory::class
        ]
    ],
    'view_manager' => [
        'template_path_stack' => [
            'album' => __DIR__ . '/../view',
        ],
    ]
);