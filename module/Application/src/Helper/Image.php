<?php
/**
 * Created by PhpStorm.
 * User: daniele
 * Date: 18/09/2017
 * Time: 00:58
 */

namespace Application\Helper;

class Image
{
    const   IMAGE_DIR = './public/images/';

    private $folder = '';

    public function __construct($folder = '')
    {
        $this->setFolder($folder);
    }

    public function setFolder ($folder) {
        $this->folder = $folder.'/';
        if(! is_dir(self::IMAGE_DIR.$folder)) {
            mkdir(self::IMAGE_DIR.$folder);
        }
        return $this;
    }

    public function archive ($requestImage) {
        $file_ext = end(explode('.', $requestImage['name']));
        $filename = sha1(uniqid(time(), true)).'.'.strtolower($file_ext);
        $file_tmp = $requestImage['tmp_name'];
        move_uploaded_file($file_tmp, self::IMAGE_DIR.$this->folder.$filename);
        return $filename;
    }
}