<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */
namespace Wall;

use Zend\Router\Http\Segment;

return [
    'router' => [
        'routes' => [
            'wall' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/:username',
                    'constraints' => [
                        'username' => '\w+'
                    ],
                    'defaults' => [
                        'controller' => Controller\WallController::class,
                        'action'     => 'index'
                    ]
                ]
            ],
            'save-wall' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/wall/:username',
                    'constraints' => [
                        'username' => '\w+'
                    ],
                    'defaults' => [
                        'controller' => Controller\WallController::class,
                        'action'     => 'save'
                    ]
                ]
            ]
        ],
    ],
    'view_manager' => [
        'template_path_stack' => [
            __DIR__ . '/../view',
        ]
    ],
    'view_helpers' => [
        'factories' => [
            'basePath' => 'public/',
        ],
    ],
];