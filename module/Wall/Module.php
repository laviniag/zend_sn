<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Wall;

use Users\Model\UsersTable;
use Wall\Model\UserImagesTable;
use Wall\Model\UserLinksTable;
use Wall\Model\UserStatusesTable;
use Wall\Model\UserCommentsTable;
use Zend\Db\Adapter\AdapterInterface;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;

class Module
{
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getControllerConfig()
    {
        return [
            'factories' => [
                Controller\WallController::class => function($container) {
                    return new Controller\WallController(
                        $container->get(UsersTable::class),
                        $container->get(UserStatusesTable::class),
                        $container->get(UserImagesTable::class),
                        $container->get(UserLinksTable::class),
                        $container->get(UserCommentsTable::class),
                        $container->get("Config")
                    );
                },
            ],
        ];
    }

    public function getServiceConfig()
    {
        return [
            'factories' => [
                Model\UserStatusesTable::class => function($container) {
                    $tableGateway = $container->get(Model\UserStatusesTableGateway::class);
                    return new Model\UserStatusesTable($tableGateway);
                },
                Model\UserStatusesTableGateway::class => function ($container) {
                    $dbAdapter = $container->get(AdapterInterface::class);
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Entity\UserStatus());
                    return new TableGateway('user_statuses', $dbAdapter, null, $resultSetPrototype);
                },
                Model\UserImagesTable::class => function($container) {
                    $tableGateway = $container->get(Model\UserImagesTableGateway::class);
                    return new Model\UserImagesTable($tableGateway);
                },
                Model\UserImagesTableGateway::class => function ($container) {
                    $dbAdapter = $container->get(AdapterInterface::class);
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Entity\UserImage());
                    return new TableGateway('user_images', $dbAdapter, null, $resultSetPrototype);
                },
                Model\UserLinksTable::class => function($container) {
                    $tableGateway = $container->get(Model\UserLinksTableGateway::class);
                    return new Model\UserLinksTable($tableGateway);
                },
                Model\UserLinksTableGateway::class => function ($container) {
                    $dbAdapter = $container->get(AdapterInterface::class);
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Entity\UserLink());
                    return new TableGateway('user_links', $dbAdapter, null, $resultSetPrototype);
                },
                Model\UserCommentsTable::class => function($container) {
                    $tableGateway = $container->get(Model\UserCommentsTableGateway::class);
                    return new Model\UserCommentsTable($tableGateway);
                },
                Model\UserCommentsTableGateway::class => function ($container) {
                    $dbAdapter = $container->get(AdapterInterface::class);
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Entity\UserComment());
                    return new TableGateway('user_comments', $dbAdapter, null, $resultSetPrototype);
                },
            ],
        ];
    }
}
