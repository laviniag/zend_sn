<?php

namespace Wall\Entity;

use Zend\Hydrator\ClassMethods;

class UserStatus
{
    const COMMENT_TYPE_ID = 1;

    protected $id = null;
    protected $userId = null;
    protected $status = null;
    protected $createdAt = null;
    protected $updatedAt = null;
    protected $comments = array();

    public function exchangeArray(array $data)
    {
        $this->id = !empty($data['id']) ? $data['id'] : null;
        $this->userId = !empty($data['user_id']) ? $data['user_id'] : null;
        $this->status = !empty($data['status']) ? $data['status'] : null;
        $this->createdAt = !empty($data['created_at']) ? $data['created_at'] : null;
        $this->updatedAt = !empty($data['updated_at']) ? $data['updated_at'] : null;
    }

    public function getArrayCopy()
    {
        $copy = [
            'id' => $this->getId(),
            'user_id' => $this->getUserId(),
            'status' => $this->getStatus(),
            'created_at' => $this->getCreatedAt(),
            'updated_at' => $this->getUpdatedAt()
        ];
        return $copy;
    }

    public function getFields($full = false)
    {
        $copy = [
            'user_id' => $this->getUserId(),
            'status' => $this->getStatus(),
            'created_at' => $this->getCreatedAt(),
        ];
        if ($full !== false) {
            $copy['id'] = $this->getId();
            $copy['updated_at'] = $this->getUpdatedAt();
        }
        return $copy;
    }

    public function setId($id)
    {
        $this->id = (int)$id;
        return $this;
    }
    
    public function setUserId($userId)
    {
        $this->userId = (int)$userId;
        return $this;
    }
    
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }
    
    public function setCreatedAt($createdAt = null)
    {
        $datetime = new \DateTime($createdAt);
        $this->createdAt = $datetime->format('Y-m-d H:i:s');
        return $this->createdAt;
    }
    
    public function setUpdatedAt($updatedAt = null)
    {
        $datetime = new \DateTime($updatedAt);
        $this->updatedAt = $datetime->format('Y-m-d H:i:s');
        return $this->updatedAt;
    }

    public function setComments($comments)
    {
        $hydrator = new ClassMethods();
        foreach ($comments as $comm) {
            $this->comments[] = $hydrator->hydrate($comm, new UserComment());
        }
    }

    public function getId()
    {
        return $this->id;
    }
    
    public function getUserId()
    {
        return $this->userId;
    }
    
    public function getStatus()
    {
        return $this->status;
    }
    
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
    
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    public function getComments()
    {
        return $this->comments;
    }

    public function hasComments()
    {
        return (!empty($this->comments)) ? true : false;
    }

    public function getType()
    {
        return self::COMMENT_TYPE_ID;
    }
}