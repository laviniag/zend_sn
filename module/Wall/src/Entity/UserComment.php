<?php

namespace Wall\Entity;

use Users\Entity\User;
use Zend\Hydrator\ClassMethods;

class UserComment
{
    protected $id = null;
    protected $userId = null;
    protected $type = null;
    protected $entryId = null;
    protected $comment = null;
    protected $createdAt = null;
    protected $updatedAt = null;
    protected $username = null;
    protected $fullname = null;
    protected $avatar = null;

    public function exchangeArray(array $data)
    {
        $this->id = !empty($data['id']) ? $data['id'] : null;
        $this->userId = !empty($data['user_id']) ? $data['user_id'] : null;
        $this->type = !empty($data['type']) ? $data['type'] : null;
        $this->entryId = !empty($data['entry_id']) ? $data['entry_id'] : null;
        $this->comment = !empty($data['comment']) ? $data['comment'] : null;
        $this->createdAt = !empty($data['created_at']) ? $data['created_at'] : null;
        $this->updatedAt = !empty($data['updated_at']) ? $data['updated_at'] : null;
    }

    public function getArrayCopy()
    {
        $copy = [
            'id' => $this->getId(),
            'user_id' => $this->getUserId(),
            'type' => $this->getType(),
            'entry_id' => $this->getEntryId(),
            'comment' => $this->getComment(),
            'created_at' => $this->getCreatedAt(),
            'updated_at' => $this->getUpdatedAt()
        ];
        return $copy;
    }

    public function toArray () {
        $copy = array();
        foreach ($this as $key => $value) {
            $copy[$key] = $value;
        }
        return $copy;
    }

    public function getFields($full = false)
    {
        $copy = [
            'user_id' => $this->getUserId(),
            'type' => $this->getType(),
            'entry_id' => $this->getEntryId(),
            'comment' => $this->getComment(),
            'created_at' => $this->getCreatedAt(),
        ];
        if ($full !== false) {
            $copy['id'] = $this->getId();
            $copy['updated_at'] = $this->getUpdatedAt();
        }
        return $copy;
    }

    public function setId($id)
    {
        $this->id = (int)$id;
        return $this;
    }
    
    public function setUserId($userId)
    {
        $this->userId = (int)$userId;
        return $this;
    }

    /**
     * @param null $type
     * @return UserComment
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @param null $entryId
     * @return UserComment
     */
    public function setEntryId($entryId)
    {
        $this->entryId = $entryId;
        return $this;
    }

    /**
     * @param null $comment
     * @return UserComment
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
        return $this;
    }

    public function setCreatedAt($createdAt = null)
    {
        $datetime = new \DateTime($createdAt);
        $this->createdAt = $datetime->format('Y-m-d H:i:s');
        return $this->createdAt;
    }
    
    public function setUpdatedAt($updatedAt = null)
    {
        $datetime = new \DateTime($updatedAt);
        $this->updatedAt = $datetime->format('Y-m-d H:i:s');
        return $this->updatedAt;
    }

    /**
     * @param $username
     * @return UserComment
     */
    public function setUsername($username)
    {
        $this->username = $username;
        return $this;
    }

    /**
     * @param $fullname
     * @return UserComment
     */
    public function setFullname($fullname)
    {
        $this->fullname = $fullname;
        return $this;
    }

    /**
     * @param null $avatar
     * @return UserComment
     */
    public function setAvatar($avatar)
    {
        $this->avatar = $avatar;
        return $this;
    }

    public function getId()
    {
        return $this->id;
    }
    
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @return null
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return null
     */
    public function getEntryId()
    {
        return $this->entryId;
    }

    /**
     * @return null
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @return null
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @return null
     */
    public function getFullname()
    {
        return $this->fullname;
    }

    /**
     * @return null
     */
    public function getAvatar()
    {
        return (!empty($this->avatar)) ? $this->avatar : 'avatar.png';
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }
    
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
}