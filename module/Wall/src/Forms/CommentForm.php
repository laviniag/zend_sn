<?php
namespace Wall\Forms;

use Wall\Validator\Spam as SpamValidator;
use Zend\Filter\StringTrim;
use Zend\Filter\StripTags;
use Zend\Form\Form;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Validator\StringLength;

class CommentForm extends Form implements InputFilterProviderInterface
{
    public function __construct(array $config)
    {
        parent::__construct('text-content');

        $this->setAttribute('method', 'post');
        $this->setAttribute('class', 'well input-append');

        $this->prepareElements();
        // Add validation rules
        $this->addInputFilter($config);
    }

    public function prepareElements()
    {
        $this->add([
            'name' => 'comment',
            'type'  => 'text',
            'attributes' => [
                'class' => 'span11',
                'placeholder' => 'Write a comment...'
            ]
        ]);
        $this->add([
            'name' => 'type',
            'type' => 'hidden',
        ]);
        $this->add([
            'name' => 'entry_id',
            'type' => 'hidden',
        ]);
        // Add the CSRF field
        $this->add([
            'type'  => 'csrf',
            'name' => 'csrf',
            'options' => [
                'csrf_options' => [
                    'timeout' => 600
                ]
            ],
        ]);
        $this->add([
            'name' => 'submit',
            'attributes' => [
                'type'  => 'submit',
                'value' => 'Add',
                'class' => 'btn btn-info'
            ]
        ]);
    }

    public function addInputFilter($config)
    {
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);
//        print_r($config);
//        die();

        // Add validation rules for the "status" field.
        $inputFilter->add([
            'name' => 'comment',
            'required' => true,
            'filters' => [
                ['name' => StripTags::class],
                ['name' => StringTrim::class],
            ],
            'validators' => [
                ['name' => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 1,
                        'max' => 255,
                    ],
                ],
                ['name' => SpamValidator::class,
                    'options' => [
                        'apiKey' => $config['apiKey'],
                        'url' => $config['url']
                    ]
                ]
            ],
        ]);
    }

    /**
     * Should return an array specification compatible with
     * {@link Zend\InputFilter\Factory::createInputFilter()}.
     *
     * @return array
     */
    public function getInputFilterSpecification()
    {
        return [
            'comment' => ['required' => true]
        ];
    }
}
