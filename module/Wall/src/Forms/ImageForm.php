<?php
namespace Wall\Forms;

use Zend\Form\Form;
use Zend\InputFilter\FileInput;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterProviderInterface;

class ImageForm extends Form implements InputFilterProviderInterface
{
    public function __construct($name = null)
    {
        parent::__construct('image-content');

        $this->setAttribute('method', 'post');
        $this->setAttribute('class', 'well input-append');
        $this->setAttribute('enctype', 'multipart/form-data');

        $this->prepareElements();
        // Add validation rules
        $this->addInputFilter();
    }

    public function prepareElements()
    {
        $this->add(array(
            'name' => 'image',
            'type' => 'file',
            'attributes' => array(
                'class' => 'span10',
            ),
        ));
        // Add the CSRF field
        $this->add([
            'type' => 'csrf',
            'name' => 'csrf',
            'options' => [
                'csrf_options' => [
                    'timeout' => 600
                ]
            ],
        ]);
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Upload',
                'class' => 'btn btn-info'
            ),
        ));
    }

    private function addInputFilter()
    {
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);

        // Add validation rules for the "image" field.
        $inputFilter->add([
            'type' => FileInput::class,
            'name' => 'image',
            'required' => true,
            'validators' => [
                ['name'    => 'FileIsImage'],
                ['name' => 'FileMimeType',
                    'options' => [
                        'mimeType' => ['image/jpeg', 'image/png', 'image/gif']
                    ]
                ],
                ['name' => 'FileImageSize',
                    'options' => [
                        'minWidth' => 128,
                        'minHeight' => 128,
                        'maxWidth' => 4096,
                        'maxHeight' => 4096
                    ]
                ],
            ],
        ]);
    }

    /**
     * Should return an array specification compatible with
     * {@link Zend\InputFilter\Factory::createInputFilter()}.
     *
     * @return array
     */
    public function getInputFilterSpecification()
    {
        return [
            'image' => ['required' => true]
        ];
    }
}
