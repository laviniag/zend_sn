<?php
namespace Wall\Model;

use Wall\Entity\UserStatus;
use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\TableGatewayInterface;

class UserStatusesTable
{
    const COMMENT_TYPE_ID = 1;
    private $tableGateway;

    public function __construct(TableGatewayInterface $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }
    
    /**
     * Method to get statuses by userId
     *
     * @param int $userId
     * @return Zend\Db\ResultSet
     */
    public function getByUserId($userId)
    {
        $select = new Select($this->tableGateway->getTable());
        $select->where(['user_id' => $userId]);
        $select->order('created_at DESC');

        $rowset = $this->tableGateway->selectWith($select);
        return $rowset;
    }
    
    /**
     * Method to insert a status to a user
     *
     * @param UserStatus $userStatus
     * @return boolean
     */
    public function create(UserStatus $userStatus)
    {
        $userStatus->setCreatedAt();
        $data = $userStatus->getFields();

        return $this->tableGateway->insert($data);
    }
}