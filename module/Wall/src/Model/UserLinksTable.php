<?php
namespace Wall\Model;

use Wall\Entity\UserLink;
use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\TableGatewayInterface;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;

class UserLinksTable
{
    const COMMENT_TYPE_ID = 3;
    private $tableGateway;

    public function __construct(TableGatewayInterface $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }
    
    /**
     * Method to get statuses by userId
     *
     * @param int $userId
     * @return Zend\Db\ResultSet
     */
    public function getByUserId($userId)
    {
        $select = new Select($this->tableGateway->getTable());
        $select->where(['user_id' => $userId]);
        $select->order('created_at DESC');

        $rowset = $this->tableGateway->selectWith($select);
        return $rowset;
    }
    
    /**
     * Method to insert a status to a user
     *
     * @param UserLink $userLink
     * @return boolean
     */
    public function create(UserLink $userLink)
    {
        $userLink->setCreatedAt();
        $data = $userLink->getFields();

        return $this->tableGateway->insert($data);
    }
}