<?php
namespace Wall\Model;

use Wall\Entity\UserComment;
use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\TableGatewayInterface;

class UserCommentsTable
{
    private $tableGateway;

    public function __construct(TableGatewayInterface $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }
    
    /**
     * Method to get statuses by userId
     *
     * @param int $userId
     * @return Zend\Db\ResultSet
     */
    public function getByUserId($userId)
    {
        $select = new Select($this->tableGateway->getTable());
        $select->where(['user_id' => $userId]);
        $select->order('created_at DESC');

        $rowset = $this->tableGateway->selectWith($select);
        return $rowset;
    }

    /**
     * Method to get statuses by userId
     *
     * @param int $type
     * @param int $entryId
     * @return Zend\Db\ResultSet
     */
    public function getByTypeAndEntryId($type, $entryId)
    {
        $select = new Select($this->tableGateway->getTable());
        $select->where(['type' => $type, 'entry_id' => $entryId]);
        $select->order('created_at DESC');

        $rowset = $this->tableGateway->selectWith($select);
        return $rowset;
    }
    
    /**
     * Method to insert a status to a user
     *
     * @param UserComment $userComment
     * @return boolean
     */
    public function create(UserComment $userComment)
    {
        $userComment->setCreatedAt();
        $data = $userComment->getFields();

        return $this->tableGateway->insert($data);
    }
}