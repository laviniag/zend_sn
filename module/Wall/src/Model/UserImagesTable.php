<?php
namespace Wall\Model;

use Wall\Entity\UserImage;
use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\TableGatewayInterface;

class UserImagesTable
{
    const COMMENT_TYPE_ID = 2;
    private $tableGateway;

    public function __construct(TableGatewayInterface $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }
    
    /**
     * Method to get statuses by userId
     *
     * @param int $userId
     * @return Zend\Db\ResultSet
     */
    public function getByUserId($userId)
    {
        $select = new Select($this->tableGateway->getTable());
        $select->where(['user_id' => $userId]);
        $select->order('created_at DESC');

        $rowset = $this->tableGateway->selectWith($select);
        return $rowset;
    }
    
    /**
     * Method to insert a status to a user
     *
     * @param UserImage $userImage
     * @return boolean
     */
    public function create(UserImage $userImage)
    {
        $userImage->setCreatedAt();
        $data = $userImage->getFields();

        return $this->tableGateway->insert($data);
    }
}