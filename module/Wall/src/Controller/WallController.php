<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Wall\Controller;

use Application\Helper\Image;
use Users\Model\UsersTable;
use Wall\Entity\UserComment;
use Wall\Entity\UserImage;
use Wall\Entity\UserLink;
use Wall\Entity\UserStatus;
use Wall\Forms\CommentForm;
use Wall\Forms\ImageForm;
use Wall\Forms\LinkForm;
use Wall\Forms\StatusForm;
use Wall\Model\UserCommentsTable;
use Wall\Model\UserImagesTable;
use Wall\Model\UserLinksTable;
use Wall\Model\UserStatusesTable;
use Zend\Dom\Query;
use Zend\Filter\FilterChain;
use Zend\Filter\StripTags;
use Zend\Filter\StringTrim;
use Zend\Filter\StripNewLines;
use Zend\Http\Client;
use Zend\Http\Request;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Session\Container;

class WallController extends AbstractActionController
{
    private $userTable;
    private $ustatusTable;
    private $uimageTable;
    private $ulinkTable;
    private $ucommentTable;
    private $akismet;

    public function __construct(UsersTable $users_table, UserStatusesTable $ustatus_table,
                                UserImagesTable $uimage_table, UserLinksTable $ulink_table,
                                UserCommentsTable $ucomment_table, $global_config)
    {
        $this->userTable = $users_table;
        $this->ustatusTable = $ustatus_table;
        $this->uimageTable = $uimage_table;
        $this->ulinkTable = $ulink_table;
        $this->ucommentTable = $ucomment_table;
        $this->akismet = $global_config['akismet'];
    }

    public function indexAction()
    {
        $viewData = array();
        $username = $this->params()->fromRoute('username');

//        $auth_session = new Container('Auth_session');
//        if (isset($auth_session->user) && $auth_session->user <> $username) {
//            return $this->redirect()->toRoute('home');
//        }
        $this->layout()->username = $username;

        $user = $this->userTable->getByUsername($username);
        if ($user === false) {
            return $this->getResponse()->setStatusCode(404);
        }

        $statusForm = new StatusForm;
        $statusForm->setAttribute('action', $this->url()
            ->fromRoute('save-wall', ['username' => $user->getUsername()]));
        $imageForm = new ImageForm();
        $imageForm->setAttribute('action', $this->url()
            ->fromRoute('save-wall', ['username' => $user->getUsername()]));
        $linkForm = new LinkForm();
        $linkForm->setAttribute('action', $this->url()
            ->fromRoute('save-wall', ['username' => $user->getUsername()]));
        $commentForm = new CommentForm($this->akismet);
        $commentForm->setAttribute('action', $this->url()
            ->fromRoute('save-wall', array('username' => $user->getUsername())));

        $userFeeds = $this->getUserFeeds($user->getId());
        $user->setFeeds($userFeeds);

        $viewData['userData'] = $user;
        $viewData['contentForm'] = $statusForm;
        $viewData['imageForm'] = $imageForm;
        $viewData['linkForm'] = $linkForm;
        $viewData['commentForm'] = $commentForm;

        return $viewData;
    }

    public function saveAction()
    {
        $this->layout('');
        $username = $this->params()->fromRoute('username');

        $user = $this->userTable->getByUsername($username);
        if ($user === false) {
            return $this->getResponse()->setStatusCode(404);
        }

        //Check if we are submitting content
        $request = $this->getRequest();

        if ($request->isPost()) {
            $result = $this->persistData ($request, $user);
            if ($result !== true) {
                $this->flashMessenger()->addErrorMessage($result);
            }
            return $this->redirect()->toRoute('wall', ['username' => $user->getUsername()]);
        }
    }

    private function getUserFeeds ($userId)
    {
        $allEntries = array(
            UserstatusesTable::COMMENT_TYPE_ID => $this->ustatusTable->getByUserId($userId)->toArray(),
            UserImagesTable::COMMENT_TYPE_ID => $this->uimageTable->getByUserId($userId)->toArray(),
            UserLinksTable::COMMENT_TYPE_ID => $this->ulinkTable->getByUserId($userId)->toArray()
        );
        $cachedUsers = array();
        foreach ($allEntries as $type => &$entries) {
            foreach ($entries as &$entry) {
                $comments = $this->ucommentTable->getByTypeAndEntryId($type, $entry['id']);
                if (count($comments) > 0) {
                    $commList = array();
                    foreach ($comments as $comm) {
                        if (array_key_exists($comm->getUserId(), $cachedUsers)) {
                            $user = $cachedUsers[$comm->getUserId()];
                        } else {
                            $user = $this->userTable->getById($comm->getUserId());
                            $cachedUsers[$comm->getUserId()] = $user;
                        }
                        $comm->setUsername($user->getUsername());
                        $comm->setFullname($user->getName().' '.$user->getSurname());
                        $comm->setAvatar($user->getAvatar());
                        $commList[] = $comm->toArray();
                    }
                    $entry['comments'] = $commList;
                }
            }
        }
        $wholeFeeds = array_merge($allEntries[UserstatusesTable::COMMENT_TYPE_ID],
            $allEntries[UserImagesTable::COMMENT_TYPE_ID], $allEntries[UserLinksTable::COMMENT_TYPE_ID]);

        usort($wholeFeeds, function($a, $b) {
            $tmpA = strtotime($a['created_at']);
            $tmpB = strtotime($b['created_at']);
            return ($tmpA == $tmpB) ? 0 : ($tmpA > $tmpB) ? -1 : 1;
        });
        return $wholeFeeds;
    }

    private function persistData ($request, $user)
    {
        $data = $request->getPost()->toArray();

        if ($data['submit'] == 'Submit' && array_key_exists('status', $data)) {
            $statusForm = new StatusForm;
            $statusForm->setData($request->getPost());
            if (! $statusForm->isValid()) {
                return $statusForm->getMessages();
            }
            $userStatus = new UserStatus();
            $userStatus->exchangeArray($statusForm->getData());
            $userStatus->setUserId($user->getId());
            $this->ustatusTable->create($userStatus);
            unset($statusForm, $userStatus);
            return true;
        }
        elseif ($data['submit'] == 'Upload') {
            if (empty($request->getFiles()->image['name'])) {
                return "No picture file selected";
            }
            $imageForm = new ImageForm();
            $data = array_merge_recursive(
                $request->getPost()->toArray(),
                $request->getFiles()->toArray()
            );
            $imageForm->setData($data);
            if (! $imageForm->isValid()) {
                return $imageForm->getMessages();
            }
            $picture = new Image($user->getUsername());
            $imageStack = $request->getFiles()->image;
            $filename = $picture->archive($imageStack);

            $userImage = new UserImage($user->getUsername());
            $userImage->setFilename($filename);
            $userImage->setUserId($user->getId());
            $this->uimageTable->create($userImage);
            unset($imageForm, $userImage, $picture);
            return true;
        }
        elseif ($data['submit'] == 'Insert' && array_key_exists('url', $data)) {
            $linkForm = new LinkForm;
            $linkForm->setData($request->getPost());
            if (! $linkForm->isValid()) {
                return $linkForm->getMessages();
            }

            $userLink = new UserLink();
            $client = new Client($data['url']);
            $client->setEncType(Client::ENC_URLENCODED);
            $client->setMethod(Request::METHOD_GET);
            $response = $client->send();

            if ($response->isSuccess()) {
                $html = $response->getBody();
                $html = mb_convert_encoding($html, 'HTML-ENTITIES', "UTF-8");

                $dom = new Query($html);
                $title = $dom->execute('title')->current()->nodeValue;
                $title = $this->filterContent ($title);
                $metatag = $dom->execute('meta');
                $descr = null;
                foreach ($metatag as $tag) {
                    if ($tag->getAttribute('name') == 'description') {
                        $descr = $tag->getAttribute('content');
                    }
                }
                $descr = $this->filterContent ($descr);

                $userLink->exchangeArray($linkForm->getData());
                $userLink->setUserId($user->getId());
                $userLink->setTitle($title);
                $userLink->setDescription($descr);
                $this->ulinkTable->create($userLink);
            }
            else {
                return "Posted link not found !";
            }
            unset($linkForm, $userLink, $client);
            return true;
        }
        elseif ($data['submit'] == 'Add' && array_key_exists('comment', $data)) {
            $commentForm = new CommentForm($this->akismet);
            $commentForm->setData($request->getPost());
            if (! $commentForm->isValid()) {
                return $commentForm->getMessages();
            }
            $userComment = new UserComment();
            $userComment->exchangeArray($commentForm->getData());
            $userComment->setUserId($user->getId());
            $this->ucommentTable->create($userComment);
            unset($commentForm, $userComment);
            return true;
        }
        return "Post failure! Please retry";
    }

    private function filterContent ($info)
    {
        if (!empty($info)) {
            $filterChain = new FilterChain();
            $filterChain->attach(new StripTags());
            $filterChain->attach(new StringTrim());
//            $filterChain->attach(new StripNewLines());
            $info = $filterChain->filter($info);
        }
        return $info;
    }
}