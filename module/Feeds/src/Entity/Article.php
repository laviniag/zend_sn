<?php

namespace Feeds\Entity;

class Article
{
    protected $id = null;
    protected $feedId = null;
    protected $title = null;
    protected $content = null;
    protected $url = null;
    protected $author = null;
    protected $createdAt = null;
    protected $updatedAt = null;
    protected $readed = null;

    public function exchangeArray(array $data)
    {
        $this->id = !empty($data['id']) ? $data['id'] : null;
        $this->feedId = !empty($data['feed_id']) ? $data['fedd_id'] : null;
        $this->title = !empty($data['title']) ? $data['title'] : null;
        $this->content = !empty($data['content']) ? $data['content'] : null;
        $this->url = !empty($data['url']) ? $data['url'] : null;
        $this->author = !empty($data['author']) ? $data['author'] : null;
        $this->createdAt = !empty($data['created_at']) ? $data['created_at'] : null;
        $this->updatedAt = !empty($data['updated_at']) ? $data['updated_at'] : null;
    }

    public function getArrayCopy()
    {
        $copy = [
            'id' => $this->getId(),
            'feed_id' => $this->getFeedId(),
            'title' => $this->getTitle(),
            'content' => $this->getContent(),
            'url' => $this->getUrl(),
            'author' => $this->getAuthor(),
            'created_at' => $this->getCreatedAt(),
            'updated_at' => $this->getUpdatedAt()
        ];
        return $copy;
    }

    public function getFields($full = false)
    {
        $copy = [
            'feed_id' => $this->getFeedId(),
            'title' => $this->getTitle(),
            'content' => $this->getContent(),
            'url' => $this->getUrl(),
            'author' => $this->getAuthor(),
            'created_at' => $this->getCreatedAt(),
        ];
        if ($full !== false) {
            $copy['id'] = $this->getId();
            $copy['updated_at'] = $this->getUpdatedAt();
        }
        return $copy;
    }

    public function setId($id)
    {
        $this->id = (int)$id;
    }
    
    public function setFeedId($feedId)
    {
        $this->feedId = (int)$feedId;
    }
    
    public function setTitle($title)
    {
        $this->title = $title;
    }
    
    public function setContent($content)
    {
        $this->content = $content;
    }
    
    public function setUrl($url)
    {
        $this->url = $url;
    }
    
    public function setAuthor($author)
    {
        $this->author = $author;
    }
    
    public function setReaded($readed)
    {
        $this->readed = $readed;
    }

    public function setCreatedAt($createdAt = null)
    {
        $datetime = new \DateTime($createdAt);
        $this->createdAt = $datetime->format('Y-m-d H:i:s');
        return $this->createdAt;
    }

    public function setUpdatedAt($updatedAt = null)
    {
        $datetime = new \DateTime($updatedAt);
        $this->updatedAt = $datetime->format('Y-m-d H:i:s');
        return $this->updatedAt;
    }
    
    public function getId()
    {
        return $this->id;
    }
    
    public function getFeedId()
    {
        return $this->feedId;
    }
    
    public function getTitle()
    {
        return $this->title;
    }
    
    public function getContent()
    {
        return $this->content;
    }
    
    public function getUrl()
    {
        return $this->url;
    }
    
    public function getAuthor()
    {
        return $this->author;
    }
    
    public function getReaded()
    {
        return $this->readed;
    }
    
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
    
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
}