<?php

namespace Feeds\Entity;

use Zend\Hydrator\ClassMethods;

class Feed
{
    protected $id = null;
    protected $userId = null;
    protected $url = null;
    protected $title = null;
    protected $icon = null;
    protected $createdAt = null;
    protected $updatedAt = null;
    protected $articles = array();

    public function exchangeArray(array $data)
    {
        $this->id = !empty($data['id']) ? $data['id'] : null;
        $this->userId = !empty($data['user_id']) ? $data['user_id'] : null;
        $this->url = !empty($data['url']) ? $data['url'] : null;
        $this->title = !empty($data['title']) ? $data['title'] : null;
        $this->icon = !empty($data['icon']) ? $data['icon'] : null;
        $this->createdAt = !empty($data['created_at']) ? $data['created_at'] : null;
        $this->updatedAt = !empty($data['updated_at']) ? $data['updated_at'] : null;
    }

    public function getArrayCopy()
    {
        $copy = [
            'id' => $this->getId(),
            'user_id' => $this->getUserId(),
            'url' => $this->getUrl(),
            'title' => $this->getTitle(),
            'icon' => $this->getIcon(),
            'created_at' => $this->getCreatedAt(),
            'updated_at' => $this->getUpdatedAt()
        ];
        return $copy;
    }

    public function getFields($full = false)
    {
        $copy = [
            'user_id' => $this->getUserId(),
            'url' => $this->getUrl(),
            'title' => $this->getTitle(),
            'icon' => $this->getIcon(),
            'created_at' => $this->getCreatedAt(),
        ];
        if ($full !== false) {
            $copy['id'] = $this->getId();
            $copy['updated_at'] = $this->getUpdatedAt();
        }
        return $copy;
    }

    public function setId($id)
    {
        $this->id = (int)$id;
    }
    
    public function setUserId($userId)
    {
        $this->userId = (int)$userId;
    }
    
    public function setUrl($url)
    {
        $this->url = $url;
    }
    
    public function setTitle($title)
    {
        $this->title = $title;
    }
    
    public function setIcon($icon)
    {
        $this->icon = $icon;
    }
    
    public function setArticles($articles)
    {
        $hydrator = new ClassMethods();
        foreach ($articles as $art) {
            $this->articles[] = $hydrator->hydrate($art, new Article());
        }
    }

    public function setCreatedAt($createdAt = null)
    {
        $datetime = new \DateTime($createdAt);
        $this->createdAt = $datetime->format('Y-m-d H:i:s');
        return $this->createdAt;
    }

    public function setUpdatedAt($updatedAt = null)
    {
        $datetime = new \DateTime($updatedAt);
        $this->updatedAt = $datetime->format('Y-m-d H:i:s');
        return $this->updatedAt;
    }
    
    public function getId()
    {
        return $this->id;
    }
    
    public function getUserId()
    {
        return $this->userId;
    }
    
    public function getUrl()
    {
        return $this->url;
    }
    
    public function getTitle()
    {
        return $this->title;
    }
    
    public function getIcon()
    {
        return $this->icon;
    }
    
    public function getArticles()
    {
        return $this->articles;
    }
    
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
    
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
}