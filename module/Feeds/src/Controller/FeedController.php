<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Feeds\Controller;

use Feeds\Entity\Feed;
use Feeds\Forms\SubscribeForm;
use Feeds\Forms\UnsubscribeForm;
use Feeds\Model\UserFeedArticlesTable;
use Feeds\Model\UserFeedsTable;
use Users\Model\UsersTable;
use Zend\Feed\Reader\Reader;
use Zend\Http\Client;
use Zend\Http\Request;
use Zend\Http\PhpEnvironment\Response;
use Zend\Dom\Query;
use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\Navigation\Navigation;
use Zend\Navigation\Page\AbstractPage;
use Zend\Paginator\Paginator;
use Zend\View\Model\JsonModel;

/**
 * This class is the responsible to answer the requests to the /feeds endpoint
 *
 * @package Wall/Controller
 */
class FeedController extends AbstractRestfulController
{
    private $userTable;
    private $ufeedsTable;
    private $uarticlesTable;

    public function __construct(UsersTable $users_table, UserFeedsTable $ufeeds_table,
                                UserFeedArticlesTable $uarticles_table)
    {
        $this->userTable = $users_table;
        $this->ufeedsTable = $ufeeds_table;
        $this->uarticlesTable = $uarticles_table;
    }

    /**
     * Get the feed list and the posts of the feed we are looking at now
     *
     * @return void
     */
    public function indexAction()
    {
        $viewData = array();
        $username = $this->params()->fromRoute('username');
        $this->layout()->username = $username;

        $currentFeedId = $this->params()->fromRoute('feed_id');

        $user = $this->userTable->getByUsername($username);
        if ($user === false) {
            return $this->getResponse()->setStatusCode(404);
        }

        $subscribeForm = new SubscribeForm();
        $unsubscribeForm = new UnsubscribeForm();
        $subscribeForm->setAttribute('action', $this->url()->fromRoute('feeds-subscribe', array('username' => $username)));
        $unsubscribeForm->setAttribute('action', $this->url()->fromRoute('feeds-unsubscribe', array('username' => $username)));

        $userFeeds = $this->getUserFeeds($user->getId());

        if ($currentFeedId === null && !empty($userFeeds)) {
            $currentFeedId = reset($userFeeds)->getId();
        }

        $feedsMenu = new Navigation();
        $router = $this->getEvent()->getRouter();
        $routeMatch = $this->getEvent()->getRouteMatch()->setParam('feed_id', $currentFeedId);
        foreach ($userFeeds as $feed) {
            $feedsMenu->addPage(
                AbstractPage::factory(array(
                    'title' => $feed->getTitle(),
                    'icon' => $feed->getIcon(),
                    'route' => 'feeds',
                    'routeMatch' => $routeMatch,
                    'router' => $router,
                    'params' => array('username' => $username, 'feed_id' => $feed->getId())
                ))
            );
        }

        $currentFeed = $currentFeedId != null? $userFeeds[$currentFeedId] : null;

        if ($currentFeed != null) {
            $paginator = new Paginator(new ArrayAdapter($currentFeed->getArticles()));
            $paginator->setItemCountPerPage(5);
            $paginator->setCurrentPageNumber($this->params()->fromRoute('page'));
            $viewData['paginator'] = $paginator;
            $viewData['feedId'] = $currentFeedId;
        }

        $unsubscribeForm->get('feed_id')->setValue($currentFeedId);

        $viewData['subscribeForm'] = $subscribeForm;
        $viewData['unsubscribeForm'] = $unsubscribeForm;
        $viewData['username'] = $username;
        $viewData['feedsMenu'] = $feedsMenu;
        $viewData['profileData'] = $user;
        $viewData['feed'] = $currentFeed;

        if ($this->flashMessenger()->hasMessages()) {
            $viewData['flashMessages'] = $this->flashMessenger()->getMessages();
        }

        return $viewData;
    }

    /**
     * Add a new subscription for the specified user
     *
     * @return void
     */
    public function subscribeAction()
    {
        $username = $this->params()->fromRoute('username');
        $request = $this->getRequest();

        if ($request->isPost()) {
            $data = $request->getPost()->toArray();

            $response = ApiClient::addFeedSubscription($username, array('url' => $data['url']));

            if ($response['result'] == TRUE) {
                $this->flashMessenger()->addMessage('Subscribed successfully!');
            } else {
                $this->flashMessenger()->addMessage($response['message']);
            }
        }

        return $this->redirect()->toRoute('feeds', array('username' => $username));
    }

    /**
     * Unsubscribe a user from a specific feed
     *
     * @return void
     */
    public function unsubscribeAction()
    {
        $username = $this->params()->fromRoute('username');
        $request = $this->getRequest();

        if ($request->isPost()) {
            $data = $request->getPost()->toArray();

            $response = ApiClient::removeFeedSubscription($username, $data['feed_id']);

            if ($response['result'] == TRUE) {
                $this->flashMessenger()->addMessage('Unsubscribed successfully!');
            } else {
                return $this->getResponse()->setStatusCode(500);
            }
        }

        return $this->redirect()->toRoute('feeds', array('username' => $username));
    }

    public function get($id)
    {
        $this->methodNotAllowed();
    }
    
    /**
     * Return a list of feed subscription for a specific user
     *
     * @return JsonModel
     */
    public function getList()
    {
//        $username = $this->params()->fromRoute('username');
        $username = 'brad';
        $user = $this->userTable->getByUsername($username);
        
        $feedsFromDb = $this->ufeedsTable->getByUserId($user->getId());
        $feeds = array();
        foreach ($feedsFromDb as $f) {
            $feeds[$f->id] = $f;
            $feeds[$f->id]['articles'] = $this->uarticlesTable->getByFeedId($f->id)->toArray();
        }
        
        return new JsonModel($feeds);
    }
    
    /**
     * Add a new subscription
     *
     * @return JsonModel
     */
    public function create($data)
    {
//        $username = $this->params()->fromRoute('username');
        $username = 'brad';
        $user = $this->userTable->getByUsername($username);

        $rssLinkXpath = '//link[@type="application/rss+xml"]';
        $faviconXpath = '//link[@rel="shortcut icon"]';
        
        $client = new Client($data['url']);
        $client->setEncType(Client::ENC_URLENCODED);
        $client->setMethod(Request::METHOD_GET);
        $response = $client->send();
        
        if ($response->isSuccess()) {
            $html = $response->getBody();
            $html = mb_convert_encoding($html, 'HTML-ENTITIES', "UTF-8"); 
            
            $dom = new Query($html);
            $rssUrl = $dom->execute($rssLinkXpath);
            
            if (!count($rssUrl)) {
                return new JsonModel(array(
                    'result' => false, 
                    'message' => 'Rss link not found in the url provided'
                ));
            }
            $rssUrl = $rssUrl->current()->getAttribute('href');
            
            $faviconUrl = $dom->execute($faviconXpath);
            if (count($faviconUrl)) {
                $faviconUrl = $faviconUrl->current()->getAttribute('href');
            } else {
                $faviconUrl = null;
            }
        } else {
            return new JsonModel(array(
                'result' => false, 
                'message' => 'Website not found'
            ));
        }

        if ($this->ufeedsTable->getVerifyUrl($user->getId(), $rssUrl)) {
            return new JsonModel(array(
                'result' => false, 
                'message' => 'You already have a subscription to this url'
            ));
        }
        
        $rss = Reader::import($rssUrl);

        $feed = new Feed;
        $feed->setUserId($user->getId());
        $feed->setUrl($rssUrl);
        $feed->setTitle($rss->getTitle());
        $feed->setIcon($faviconUrl);
        $result = $this->ufeedsTable->create($feed);

        return new JsonModel(['result' => $result]);
    }
    
    /**
     * Method not available for this endpoint
     *
     * @return void
     */
    public function update($id, $data)
    {
        $this->methodNotAllowed();
    }
    
    /**
     * Delete a subscription
     *
     * @return JsonModel
     */
    public function delete($id)
    {
//        $username = $this->params()->fromRoute('username');
        $username = 'brad';
        $user = $this->userTable->getByUsername($username);

        $this->uarticlesTable->delete(array('feed_id' => $id));
        return new JsonModel(array(
            'result' => $this->ufeedsTable->delete(array('id' => $id, 'user_id' => $user->id))
        ));
    }
    
    protected function methodNotAllowed()
    {
        $this->response->setStatusCode(Response::STATUS_CODE_405);
    }
}