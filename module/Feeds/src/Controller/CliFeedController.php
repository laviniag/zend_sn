<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Feeds\Controller;

use Feeds\Model\UserFeedArticlesTable;
use Feeds\Model\UserFeedsTable;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Feed\Reader\Reader;

/**
 * This class is the responsible to process the feeds and retrieve new articles
 *
 * @package Wall/Controller
 */
class CliFeedController extends AbstractActionController
{
    private $ufeedsTable;
    private $uarticlesTable;

    public function __construct(UserFeedsTable $ufeeds_table, UserFeedArticlesTable $uarticles_table)
    {
        $this->ufeedsTable = $ufeeds_table;
        $this->uarticlesTable = $uarticles_table;
    }
    
    public function processFeedsAction()
    {
        $request = $this->getRequest();
        $verbose = $request->getParam('verbose') || $request->getParam('v');

        $feeds = $this->ufeedsTable->select();
        
        foreach ($feeds as $feed) {
            if ($verbose) {
                printf("Processing feed: %s\n", $feed['url']);
            }
            
            $lastUpdate = strtotime($feed['updated_at']);
            $rss = Reader::import($feed['url']);
            
            // Loop over each channel item/entry and store relevant data for each
            foreach ($rss as $item) {
                $timestamp = $item->getDateCreated()->getTimestamp();
                if ($timestamp > $lastUpdate) {
                    if ($verbose) {
                        printf("Processing item: %s\n", $item->getTitle());
                    }
                    $author = $item->getAuthor();
                    if (is_array($author)) {
                        $author = $author['name'];
                    }
                    
                    $this->uarticlesTable->create($feed['id'], $item->getTitle(), $item->getContent(), $item->getLink(), $author);
                }
            }
            
            if ($verbose) {
                printf("Updating timestamp\n");
            }
            
            $this->ufeedsTable->updateTimestamp($feed['id']);
            
            if ($verbose) {
                printf("Finished feed processing\n\n");
            }
        }
    }
}