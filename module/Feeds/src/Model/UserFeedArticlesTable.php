<?php
namespace Feeds\Model;

use Feeds\Entity\Article;
use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\TableGatewayInterface;

class UserFeedArticlesTable
{
    private $tableGateway;

    public function __construct(TableGatewayInterface $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    /**
     * Method to get rows by feed_id
     *
     * @param int $feedId
     * @return Zend\Db\ResultSet
     */
    public function getByFeedId($feedId)
    {
        $select = new Select($this->tableGateway->getTable());
        $select->where(['feed_id' => $feedId]);
        $select->order('created_at DESC');

        $rowset = $this->tableGateway->selectWith($select);
        return $rowset;
    }
    
    /**
     * Method to add an item
     *
     * @param int $userId
     * @param string $title
     * @param string $content
     * @param string $url
     * @return boolean
     */
    public function create(Article $article, $feedId, $title, $content, $url, $author)
    {
        $article->setCreatedAt();
        $data = $article->getFields();

        return $this->tableGateway->insert($data);
    }
}