<?php
namespace Feeds\Model;

use Feeds\Entity\Feed;
use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\TableGatewayInterface;

class UserFeedsTable
{
    private $tableGateway;

    public function __construct(TableGatewayInterface $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    /**
     * Method to get statuses by userId
     *
     * @param int $userId
     * @return Zend\Db\ResultSet
     */
    public function getByUserId($userId)
    {
        $select = new Select($this->tableGateway->getTable());
        $select->where(['user_id' => $userId]);
        $select->order('created_at DESC');

        $rowset = $this->tableGateway->selectWith($select);
        return $rowset;
    }

    /**
     * Method to get statuses by userId
     *
     * @param int $userId
     * @param string $url
     * @return boolean
     */
    public function getVerifyUrl($userId, $url)
    {
        $rowset = $this->tableGateway->select(['user_id' => $userId, 'url' => $url]);
        return ($rowset->count() == 1) ? true : false;
    }

    /**
     * Method to add a subscription to a rss feed
     *
     * @param int $userId
     * @param string $url
     * @param string $title
     * @return boolean
     */
    public function create(Feed $feed)
    {
        $feed->setCreatedAt();
        $data = $feed->getFields();

        return $this->tableGateway->insert($data);
    }
    
    /**
     * Update the feed updated_at field to reflect when we get the entries
     *
     * @param int $feedId 
     * @return int
     */
    public function updateTimestamp($feedId)
    {

    }
}