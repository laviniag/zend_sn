<?php
/**
 * Zend Framework (http://framework.zend.com/],
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c], 2005-2012 Zend Technologies USA Inc. (http://www.zend.com],
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */
namespace Feeds;

use Zend\Router\Http\Segment;

return [
    'router' => [
        'routes' => [
            'feeds' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/:username/feeds[/:feed_id][/page/:page]',
                    'constraints' => [
                        'username' => '\w+',
                        'feed_id' => '\d*',
                    ],
                    'defaults' => [
                        'controller' => Controller\FeedController::class,
                        'action' => 'index',
                        'page' => 1
                    ],
                ],
            ],
            'feeds-subscribe' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/:username/feeds/subscribe',
                    'constraints' => [
                        'username' => '\w+',
                    ],
                    'defaults' => [
                        'controller' => Controller\FeedController::class,
                        'action' => 'subscribe'
                    ],
                ],
            ],
            'feeds-unsubscribe' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/:username/feeds/unsubscribe',
                    'constraints' => [
                        'username' => '\w+',
                    ],
                    'defaults' => [
                        'controller' => Controller\FeedController::class,
                        'action' => 'unsubscribe'
                    ],
                ],
            ],
        ],
    ],
    'console' => [
        'router' => [
            'routes' => [
                'feeds-process' => [
                    'options' => [
                        'route' => 'feeds process [--verbose|-v]',
                        'defaults' => [
                            'controller' => Controller\FeedController::class,
                            'action'     => 'processFeeds'
                        ],
                    ],
                ],
            ],
        ],
    ]
];