<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Feeds;

use Feeds\Model\UserFeedArticlesTable;
use Feeds\Model\UserFeedsTable;
use Users\Model\UsersTable;

class Module
{
    /**
     * Convenience method to return the config file
     *
     * @return string
     */
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getControllerConfig()
    {
        return [
            'factories' => [
                Controller\FeedController::class => function($container) {
                    return new Controller\FeedController(
                        $container->get(UsersTable::class),
                        $container->get(UserFeedsTable::class),
                        $container->get(UserFeedArticlesTable::class)
                    );
                },
                Controller\CliFeedController::class => function($container) {
                    return new Controller\CliFeedController(
                        $container->get(UserFeedsTable::class),
                        $container->get(UserFeedArticlesTable::class)
                    );
                },
            ],
        ];
    }

    public function getServiceConfig()
    {
        return [
            'factories' => [
                Model\UserFeedsTable::class => function($container) {
                    $tableGateway = $container->get(Model\UserFeedsTableGateway::class);
                    return new Model\UserFeedsTable($tableGateway);
                },
                Model\UserFeedsTableGateway::class => function ($container) {
                    $dbAdapter = $container->get(AdapterInterface::class);
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Entity\Feed());
                    return new TableGateway('user_feeds', $dbAdapter, null, $resultSetPrototype);
                },
                Model\UserFeedArticlesTable::class => function($container) {
                    $tableGateway = $container->get(Model\UserFeedArticlesTableGateway::class);
                    return new Model\UserFeedArticlesTable($tableGateway);
                },
                Model\UserFeedArticlesTableGateway::class => function ($container) {
                    $dbAdapter = $container->get(AdapterInterface::class);
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Entity\Article());
                    return new TableGateway('user_feed_articles', $dbAdapter, null, $resultSetPrototype);
                },
            ],
        ];
    }
}